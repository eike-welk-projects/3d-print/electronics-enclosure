/******************************************************************************
            Attachment Points - Electronics Enclosure
*******************************************************************************

This file contains attachment points for the components inside the enclosure,
and for feet on the outside.

This is part of an electronics enclosure.
*/


// use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>
// use <body-join-end.scad>
use <end-join.scad>
// use <body-stringer.scad>


$fa = 2;
$fs = 0.25;

module attachment_point_matter(
    // possible values "inner", "i", "outer", "o"
    position="inner",
    screw_diameter=2.9,
    head_diameter=5.5,
    rib_height = 3,
    rib_pitch = 6,
    shell_wall_thickness = 2,
) {
    // The cylindrical part
    post_outer_d = head_diameter * 1.2;
    screw_bite_min_l = screw_bite_min_length(screw_diameter);

    if (position == "inner" || position == "i") {
        post_l = max(rib_height, screw_bite_min_l);
        translate([0, 0, shell_wall_thickness])
        cylinder(h=post_l, d=post_outer_d);
    } else {
        post_l = max(rib_height + shell_wall_thickness, screw_bite_min_l);
        cylinder(h=post_l, d=post_outer_d);
    }

    // The little stringer
    stringer_l = 3.0 * rib_pitch;

    translate([0, 0, shell_wall_thickness])
    translate([0, 0, rib_height/2])
    cube([stringer_l, shell_wall_thickness, rib_height], center=true);
}
// attachment_point_matter(position="i");
// attachment_point_matter(position="o");


module attachment_point_hole(
    // possible values "inner", "i", "outer", "o"
    position="inner",
    screw_diameter=2.9,
    head_diameter=5.5,
    rib_height = 3,
    rib_pitch = 6,
    shell_wall_thickness = 2,
) {
    ds = 0.02;

    screw_bite_d = screw_bite_diameter(screw_diameter);
    screw_bite_min_l = screw_bite_min_length(screw_diameter);

    // The cylindrical part
    if (position == "inner" || position == "i") {
        post_l = max(rib_height, screw_bite_min_l) + ds;
        translate([0, 0, shell_wall_thickness - ds/2])
        cylinder(h=post_l, d=screw_bite_d);
    } else {
        post_l = max(rib_height + shell_wall_thickness, screw_bite_min_l) + ds;
        translate([0, 0, -ds/2])
        cylinder(h=post_l, d=screw_bite_d);
    }
}
// attachment_point_hole(position="i");
// attachment_point_hole(position="o");


/* The space where attachment points can be generated. */
module attachment_point_possible_space(
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_wall_thickness = 2,
) {
    ds = 0.02;
    end_l = get_end_length(shell_wall_thickness);

    // The cube is slightly higher/lower so that the holes are not cut off.
    translate([end_l, 0, -ds/2])
    cube([shell_length - 2*end_l, shell_width, shell_height + ds]);
}
// #attachment_point_possible_space();

/* Generate the attachment points on the lower part of the body.

Matter and holes need to be generated separately
*/
module attachment_point_arrangement(
    // The attachment points
    attachment_points=[
        [20, 20, "o"],
        [20, 80, "o"],
        [40, 30, "i"],
        [40, 70, "i"],
    ],
    // Generate matter or holes. Possible values: "matter", "holes"
    type="matter",
    // Relevant dimensions of the screws
    screw_diameter=2.9,
    head_diameter=5.5,
    // Properties of the ribs
    rib_height = 3,
    rib_pitch = 6,
    // Dimensions of the body shell where the points are located
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_wall_thickness = 2,
) {
    intersection() {
        // Generate the attachment points
        for (i = [0 : 1 : len(attachment_points) - 1]) {
            pt = attachment_points[i];

            translate([pt.x, pt.y, 0])
            if (type == "matter") {
                attachment_point_matter(
                    position=pt[2],
                    screw_diameter=screw_diameter,
                    head_diameter=head_diameter,
                    rib_height = rib_height,
                    rib_pitch = rib_pitch,
                    shell_wall_thickness = shell_wall_thickness
                );
            } else {
                attachment_point_hole(
                    position=pt[2],
                    screw_diameter=screw_diameter,
                    head_diameter=head_diameter,
                    rib_height = rib_height,
                    rib_pitch = rib_pitch,
                    shell_wall_thickness = shell_wall_thickness
                );
            }
        }

        attachment_point_possible_space(
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_wall_thickness = shell_wall_thickness
        );
    }
}
// attachment_point_arrangement(type="matter");
// attachment_point_arrangement(type="hole");


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {

        body_shell_outer(shell_wall_thickness=2);
        body_shell_inner(shell_wall_thickness=2);
    }
difference() {
    attachment_point_arrangement(type="matter");
    attachment_point_arrangement(type="hole");
}
}
test_geometry();
