/******************************************************************************
                Longitudinal Pattern - Electronics Enclosure
*******************************************************************************

This file contains longitudinal patterns for vent holes and ribs.

This is part of an electronics enclosure.
*/


use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>
use <end-join.scad>


$fa = 2;
$fs = 0.25;


/* A horizontal opening for a vent */
module opening_horizontal(
    opening_width=3,
    // Possible values: "bottom", "top"
    position = "bottom",
    // Center the opening in X-Direction
    center=false,
    // Dimensions of the body shell where the opening is located
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    tz = position == "top"
       ? shell_height - shell_wall_thickness
       : 0;
    tx = center ? -opening_width/2 : 0;

    translate([tx, 0, tz])
    translate([opening_width, shell_radius, -0.01])
    rotate([0, 0, 90])
    rounded_bar(
        shell_width - 2*shell_radius,
        opening_width,
        shell_wall_thickness + 0.02
        );
}
// opening_horizontal();


/* A vertical opening for a vent */
module opening_vertical(
    opening_width=3,
    // Possible values: "front", "rear"
    position = "front",
    // Center the opening in X-Direction
    center=false,
    // Dimensions of the body shell where the opening is located
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    ty = position == "rear"
       ? shell_width - shell_wall_thickness
       : 0;
    tx = center ? -opening_width/2 : 0;

    translate([tx, ty, 0])
    translate([opening_width, shell_wall_thickness + 0.01, shell_radius])
    rotate([0, -90, 90])
    rounded_bar(
        shell_height - 2*shell_radius,
        opening_width,
        shell_wall_thickness + 0.02
        );
}
// opening_vertical();


/* Four vent openings on all four sides of the shell. */
module opening_quad(
    opening_width=3,
    // Center the opening in X-Direction
    center=false,
    // Dimensions of the body shell where the opening is located
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    opening_horizontal(
        opening_width=opening_width, position = "bottom",
        center=center,
        shell_width=shell_width, shell_height=shell_height,
        shell_radius=shell_radius, shell_wall_thickness=shell_wall_thickness
    );
    opening_horizontal(
        opening_width=opening_width, position = "top",
        center=center,
        shell_width=shell_width, shell_height=shell_height,
        shell_radius=shell_radius, shell_wall_thickness=shell_wall_thickness
    );
    opening_vertical(
        opening_width=opening_width, position = "front",
        center=center,
        shell_width=shell_width, shell_height=shell_height,
        shell_radius=shell_radius, shell_wall_thickness=shell_wall_thickness
    );
    opening_vertical(
        opening_width=opening_width, position = "rear",
        center=center,
        shell_width=shell_width, shell_height=shell_height,
        shell_radius=shell_radius, shell_wall_thickness=shell_wall_thickness
    );
}
// opening_quad();


/* Create regularly spaced vents in all four sides of the body. */
module vent_all(
    // Parameters of the vents
    opening_width=3,
    opening_pitch=5,
    // This much space at the body's end is free of vents
    end_dist=10,
    // Parameters of the body shell
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=2
) {
    vent_l = shell_length - 2 * end_dist;
    vent_pos = regular_pattern_centered(length=vent_l, pitch=opening_pitch);

    translate([end_dist, 0, 0])
    for( x = vent_pos ) {
        translate([x, 0, 0])
        opening_quad(
            opening_width=opening_width,
            center=true,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );
    }
}
// vent_all();


/* Create ribs between the regularly spaced vents on the inside of the body. */
module ribs_between_vents(
    // Dimensions of the rib itself
    width=2,
    height=5,
    // Parameters of the vents
    opening_width=3,
    opening_pitch=5,
    // This much space at the body's end is free of ribs
    end_dist=10,
    // Parameters of the body shell
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=2
) {
    vent_l = shell_length - 2 * end_dist;
    vent_pos = regular_pattern_centered(length=vent_l, pitch=opening_pitch);
    rib_pos = pattern_in_between(vent_pos);

    translate([end_dist, 0, 0])
    for( x = rib_pos ) {
        translate([x, 0, 0])
        rib(
            width=width,
            height=height,
            center=true,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );
    }
}
// ribs_between_vents();


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {
        body_shell_outer(shell_wall_thickness=2);
        body_shell_inner(shell_wall_thickness=2);
    }

    ow = 20;
    op = 25;
    // Vent
    vent_all(
        opening_width=ow,
        opening_pitch=op
    );
    // Small Ribs
    ribs_between_vents(
        opening_width=ow,
        opening_pitch=op
    );

//    opening_quad(center=true);
//    rib(center=true);
}
test_geometry();
