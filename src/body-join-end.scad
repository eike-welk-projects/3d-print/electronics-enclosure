/******************************************************************************
       Elements for Joining the Parts at the Ends - Electronics Enclosure
*******************************************************************************

Here are geometric elements for joining the body parts together at the ends.

This is part of an electronics enclosure.
*/


// use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>
use <body-join-screw.scad>
use <end-join.scad>


$fa = 2;
$fs = 0.25;


/* The material of the features to joint the body halves together at the end. */
module body_end_matter(
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    // Possible values: "left", "right"
    position = "left",
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=2.0
) {
    end_length = get_end_length(shell_wall_thickness);

    mx = position == "left"
       ? 0
       : 1;

    tx = position == "left"
       ? 0
       : shell_length;

    // Translate and mirror the end section, when it is realized on the other
    // end of the enclosure.
    translate([tx, 0, 0])
    mirror([mx, 0, 0]) {
        // A regular rib, to distribute the clamping force.
        translate([end_length, 0, 0])
        rib(
            width=shell_wall_thickness,
            height=get_rib_height(shell_width, shell_height),
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );

        // Two screws hold the parts together
        // Front
        body_screw_translation(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            position = "front-left",
            end_dist = end_length,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        )
        body_screw_column(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            position = "front-left",
            end_dist = end_length,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );
        // Rear
        body_screw_translation(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            position = "rear-left",
            end_dist = end_length,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        )
        body_screw_column(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            position = "rear-left",
            end_dist = end_length,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );
    }
}


/* The holes of the features to joint the body halves together at the end. */
module body_end_holes(
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    position = "left",
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=2.0
) {
    end_length = get_end_length(shell_wall_thickness);

    mx = position == "left"
       ? 0
       : 1;

    tx = position == "left"
       ? 0
       : shell_length;

    translate([tx, 0, 0])
    mirror([mx, 0, 0]) {
        // Two screw holes hold the parts together
        // Front
        body_screw_translation(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            position = "front-left",
            end_dist = end_length,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        )
        body_screw_hole(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            position = "front-left",
            end_dist = end_length,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );
        // Rear
        body_screw_translation(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            position = "rear-left",
            end_dist = end_length,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        )
        body_screw_hole(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            position = "rear-left",
            end_dist = end_length,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );
    }

}


// Test geometry
module test_geometry() {
    // end_pos = "left";
    end_pos = "right";

    difference() {
        union() {
            // create the shell
            %difference() {
                body_shell_outer(shell_wall_thickness=2.0);
                body_shell_inner(shell_wall_thickness=2.0);
            }

            body_end_matter(position=end_pos);
        }

        body_end_holes(position=end_pos);
    }
}
test_geometry();
