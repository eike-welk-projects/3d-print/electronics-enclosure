/******************************************************************************
            Flat End Plate - Electronics Enclosure
*******************************************************************************

This file contains an end plate for the electronics enclosure. This end plate is
flat.

This is part of an electronics enclosure.
*/


// use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>
// use <body-join-end.scad>
use <end-join.scad>
// use <body-stringer.scad>
use <end-plate-hole.scad>


$fa = 2;
$fs = 0.25;


/* An end plate for the enclosure that is flat. */
module end_plate_flat(
    holes=[],
    position = "left",
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=2,
) {
    rib_height = get_rib_height(shell_width, shell_height);
    plate_thickness = max(rib_height, shell_wall_thickness);
    // plate_thickness = shell_wall_thickness;

    tx = position == "right" ? shell_length : 0;
    mx = position == "right" ? 1 : 0;

    translate([tx, 0, 0])
    mirror([mx, 0, 0])
    difference() {
        union() {
            // The end plate itself
            translate([-plate_thickness, 0, 0])
            body_shell_outer(
                length=plate_thickness,
                width=shell_width,
                height=shell_height,
                radius=shell_radius,
                shell_wall_thickness=shell_wall_thickness
            );

            // The geometry to join the plate to the body
            end_join_matter_plate(
                shell_length=shell_length,
                shell_width=shell_width,
                shell_height=shell_height,
                shell_radius=shell_radius,
                shell_wall_thickness=shell_wall_thickness
            );
        }

        end_hole_arrangement(
            // The attachment points
            holes=holes,
            // Properties of the ribs
            plate_thickness = plate_thickness
        );
    }
}
// end_plate_flat(position = "left");
// end_plate_flat(position = "right");
// end_plate_flat(
//     holes=[
//         [50, 30, "rect" , 20, 10],
//         [30, 15, "round",  5,   ],
//         [70, 15, "round",  5,   ],
//     ]
// );


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {
        body_shell_outer(shell_wall_thickness=2);
        body_shell_inner(shell_wall_thickness=2);
    }

    end_plate_flat(
        holes=[
            [50, 30, "rect" , 20, 10],
            [30, 15, "round",  5,   ],
            [70, 15, "round",  5,   ],
        ],
        position="left"
    );
    end_plate_flat(position = "right");
}
test_geometry();
