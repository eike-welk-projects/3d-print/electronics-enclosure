/******************************************************************************
      Geometry to join the end caps to the body. - Electronics Enclosure
*******************************************************************************

In this file are geometric elements to join the end caps to the body.

This is part of an electronics enclosure.
*/


use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>


$fa = 2;
$fs = 0.25;


/* Compute the total length of the geometry to join the end caps. */
function get_end_length(shell_wall_thickness) =
    2.5 * shell_wall_thickness;


module shell_inner_centered_2d(
    shell_width=100,
    shell_height=50,
    shell_radius=5,
    shell_wall_thickness=2
) {
    ds = 0.01;

    projection()
    rotate([0, -90, 0])
    translate([0, -shell_width/2, -shell_height/2])
    body_shell_inner (
        length=1,
        width=shell_width,
        height=shell_height,
        radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness - 0.01
    );
}
// shell_inner_centered_2d();


// /* A small tooth that keeps the end plates in place. */
module end_join_tooth(
    position = "left",
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=5,
    shell_wall_thickness=2
) {
    tooth_height = shell_wall_thickness * 2.0;
    // Compute the scale factors for the extrusions
    sc_w = (shell_width - tooth_height)/shell_width;
    sc_h = (shell_height - tooth_height)/shell_height;

    ds = 0.02;
    exh = shell_wall_thickness/2 * 1.5;

    tx = position == "right" ? shell_length : 0;
    mx = position == "right" ? 1 : 0;

    module shell_inner_2d() {
        shell_inner_centered_2d(
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness - ds
        );
    }

    translate([tx, 0, 0])
    mirror([mx, 0, 0])
    translate([0, shell_width/2, shell_height/2])
    rotate([0, 90, 0])
    difference() {
        // the base of the tooth
        translate([0, 0, ds/2])
        linear_extrude(exh * 2 - ds)
        shell_inner_2d();

        // The pointy part of the tooth
        // Sloped inner part of the tooth
        translate([0, 0, exh])
        linear_extrude(exh, scale=[1/sc_h, 1/sc_w])
        scale([sc_h, sc_w])
        shell_inner_2d();
        // horizontal outer part of the tooth
        linear_extrude(exh + ds)
        scale([sc_h, sc_w])
        shell_inner_2d();
    }

}
// end_join_tooth(position = "left");
end_join_tooth(position = "right");


/* Two small teeth, at both ends of the body, that keep the end plates in place. */
module end_join_matter_body(
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=5,
    shell_wall_thickness=2
) {
    end_join_tooth(
        position = "left",
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
    end_join_tooth(
        position = "right",
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
};


/* A thick rib with an indentation on the outside, for a (the) small tooth to
 * bite into. */
module end_join_matter_plate(
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=5,
    shell_wall_thickness=2
) {
    ds = 0.01;

    difference() {
        translate([-shell_wall_thickness, 0, 0])
        rib(
            width=shell_wall_thickness * 3.3,
            height=shell_wall_thickness * 2.5,
            center=false,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness - ds
        );

        end_join_tooth(
            position = "left",
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness - ds
        );
    }
}
end_join_matter_plate();

// Test geometry
module test_geometry() {
    // end_pos = "left";
    end_pos = "right";
    shell_length=200;
    shell_width=100;
    shell_height=50;
    shell_radius=5;
    shell_wall_thickness=2;

    difference() {
        union() {
            // create the shell
            %difference() {
                body_shell_outer(
                    length=shell_length,
                    width=shell_width,
                    height=shell_height,
                    radius=shell_radius,
                    shell_wall_thickness=shell_wall_thickness
                );
                body_shell_inner(
                    length=shell_length,
                    width=shell_width,
                    height=shell_height,
                    radius=shell_radius,
                    shell_wall_thickness=shell_wall_thickness
                );
            }

            // end_join_matter_body(
            //     shell_length=shell_length,
            //     shell_width=shell_width,
            //     shell_height=shell_height,
            //     shell_radius=shell_radius,
            //     shell_wall_thickness=shell_wall_thickness
            // );

            // end_join_matter_plate(
            //     shell_length=shell_length,
            //     shell_width=shell_width,
            //     shell_height=shell_height,
            //     shell_radius=shell_radius,
            //     shell_wall_thickness=shell_wall_thickness
            // );
        }

    }
}
test_geometry();
