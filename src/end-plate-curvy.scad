/******************************************************************************
            Curvy End Plate - Electronics Enclosure
*******************************************************************************

This file contains an end plate for the electronics enclosure. This end plate is
curved in 3 dimensions.

This is part of an electronics enclosure.
*/


// use <helpers.scad>
use <boxes.scad>
use <base-geometry.scad>
// use <body-join-end.scad>
use <end-join.scad>
// use <body-stringer.scad>
use <end-plate-hole.scad>


$fa = 2;
$fs = 0.25;


/* An end plate for the enclosure that is curvy. */
module end_plate_curvy(
    holes=[],
    position = "left",
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=2,
) {
    ds = 0.02;

    plate_thickness = get_rib_height(shell_width, shell_height);
    inner_r_raw = shell_radius - plate_thickness;
    inner_r = max(inner_r_raw, body_shell_inner_radius_min());


    tx = position == "right" ? shell_length : 0;
    mx = position == "right" ? 1 : 0;

    translate([tx, 0, 0])
    mirror([mx, 0, 0])
    difference() {
        union() {
            // The end plate itself
            translate([0, shell_width/2, shell_height/2])
            difference() {
                // The base shape
                roundedCube(
                    [2 * shell_radius, shell_width, shell_height],
                    r=shell_radius,
                    center=true
                );

                // The hollow center
                roundedCube(
                    [
                        2 * inner_r,
                        shell_width - 2*plate_thickness,
                        shell_height - 2*plate_thickness
                    ],
                    r=inner_r,
                    center=true
                );
                // Cut off the right half
                translate([shell_radius, 0, 0])
                cube(
                    [2 * shell_radius, shell_width + ds, shell_height + ds],
                    center=true
                );
            }

            // The geometry to join the plate to the body
            end_join_matter_plate(
                shell_length=shell_length,
                shell_width=shell_width,
                shell_height=shell_height,
                shell_radius=shell_radius,
                shell_wall_thickness=shell_wall_thickness
            );
        }

        translate([-shell_radius, 0, 0])
        end_hole_arrangement(
            // The attachment points
            holes=holes,
            // Properties of the ribs
            plate_thickness = plate_thickness
        );
    }
}
// end_plate_curvy(position = "left");
// end_plate_curvy(position = "right");
end_plate_curvy(
    holes=[
        [50, 30, "rect" , 20, 10],
        [30, 15, "round",  5,   ],
        [70, 15, "round",  5,   ],
    ]
);


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {
        body_shell_outer(shell_wall_thickness=2);
        body_shell_inner(shell_wall_thickness=2);
    }

}
test_geometry();
