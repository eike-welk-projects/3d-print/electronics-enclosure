/******************************************************************************
            The body of the enclosure - Electronics Enclosure
*******************************************************************************

Here is the body of the enclosure.

This is part of an electronics enclosure.
*/


use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>
use <body-join-end.scad>
use <body-join.scad>
use <end-join.scad>
use <body-longitudinal-pattern.scad>
use <body-stringer.scad>
use <body-attachment-point.scad>


$fa = 2;
$fs = 0.25;

/* The body of the enclosure as one piece.

This module is later cut into two pieces, to obtain the top and bottom parts.
*/
module body_whole(
    // Points where components are screwed to the lower part of the body.
    attachment_points=[[20, 20, "o"], [40, 30, "i"],],
    // Properties of the screws
    // Diameter of the screws that hold the parts together.
    screw_diameter=2.9,
    // Diameters of the screw heads.
    head_diameter=5.5,
    // Shape of the body
    // Height where the top and bottom halves are split apart.
    partition_height = 12,
    // Length of the body (without the ends).
    shell_length=200,
    // Width of the body
    shell_width=100,
    // Height of the body
    shell_height=50,
    // Radius at the edges of the body
    shell_radius=5,
    // Thickness of the walls. Also thickness of ribs and stringers.
    shell_wall_thickness=2
) {
    vent_opening_width = 3;
    vent_opening_pitch = 6;

    rib_height = get_rib_height(shell_width, shell_height);
    stringer_height = rib_height;
    end_dist = get_end_length(shell_wall_thickness) + shell_wall_thickness;

    echo("Space available in the body:");
    echo("----------------------------");
    available_x = shell_length - 2 * get_end_length(shell_wall_thickness);
    available_y = shell_width - 2 * (shell_wall_thickness + max(rib_height, stringer_height));
    available_z = shell_height
                - (shell_wall_thickness + max(rib_height, stringer_height))
                - (shell_wall_thickness + max(rib_height, stringer_height, 2.5 * screw_diameter));
    echo("X:", available_x);
    echo("Y:", available_y);
    echo("Z:", available_z);
    echo();

    intersection() {
        difference() {
            union() {
                difference() {
                    // The overall matter, the rounded corners are removed last.
                    cube([shell_length, shell_width, shell_height]);

                    // Antimatter / Voids below
                    // The inner space
                    body_shell_inner (
                        length=shell_length,
                        width=shell_width,
                        height=shell_height,
                        radius=shell_radius,
                        shell_wall_thickness=shell_wall_thickness
                    );

                    // Vents
                    // translate([end_dist, 0, 0])
                    vent_all(
                        opening_width=vent_opening_width,
                        opening_pitch=vent_opening_pitch,
                        end_dist = end_dist,
                        shell_length=shell_length,
                        shell_width=shell_width,
                        shell_height=shell_height,
                        shell_radius=shell_radius,
                        shell_wall_thickness=shell_wall_thickness
                    );
                }

                // Geometry to join the two parts of the body, except the holes.
                body_join_matter(
                    screw_diameter=screw_diameter,
                    head_diameter=head_diameter,
                    partition_height = partition_height,
                    shell_length=shell_length,
                    shell_width=shell_width,
                    shell_height=shell_height,
                    shell_radius=shell_radius,
                    shell_wall_thickness=shell_wall_thickness
                );

                // Geometry to join the end caps to the body.
                end_join_matter_body(
                    shell_length=shell_length,
                    shell_width=shell_width,
                    shell_height=shell_height,
                    shell_radius=shell_radius,
                    shell_wall_thickness=shell_wall_thickness
                );

                // Lengthwise reinforcements
                stringer_all(
                    width=shell_wall_thickness,
                    height=stringer_height,
                    pitch=get_stringer_pitch(),
                    end_dist = end_dist,
                    partition_height=partition_height,
                    shell_length=shell_length,
                    shell_width=shell_width,
                    shell_height=shell_height,
                    shell_radius=shell_radius,
                    shell_wall_thickness=shell_wall_thickness
                );

                // Ribs between the vent holes
                ribs_between_vents(
                    // Dimensions of the rib itself
                    width=shell_wall_thickness,
                    height=rib_height,
                    // Parameters of the vents
                    opening_width=vent_opening_width,
                    opening_pitch=vent_opening_pitch,
                    // This much space at the body's end is free of ribs
                    end_dist=end_dist,
                    // Parameters of the body shell
                    shell_length=shell_length,
                    shell_width=shell_width,
                    shell_height=shell_height,
                    shell_radius=shell_radius,
                    shell_wall_thickness=shell_wall_thickness
                );

                // Attachment points to screw in components
                attachment_point_arrangement(
                    // The attachment points
                    attachment_points=attachment_points,
                    // Generate matter or holes. Possible values: "matter", "holes"
                    type="matter",
                    // Relevant dimensions of the screws
                    screw_diameter=screw_diameter,
                    head_diameter=head_diameter,
                    // Properties of the ribs
                    rib_height = rib_height,
                    rib_pitch = vent_opening_pitch,
                    // Dimensions of the body shell where the points are located
                    shell_length=shell_length,
                    shell_width=shell_width,
                    shell_height=shell_height,
                    shell_wall_thickness = shell_wall_thickness
                );
            }

            // The screw holes
            body_join_holes(
                screw_diameter=screw_diameter,
                head_diameter=head_diameter,
                partition_height = partition_height,
                shell_length=shell_length,
                shell_width=shell_width,
                shell_height=shell_height,
                shell_radius=shell_radius,
                shell_wall_thickness=shell_wall_thickness
            );

            // The holes of the attachment points
            attachment_point_arrangement(
                // The attachment points
                attachment_points=attachment_points,
                // Generate matter or holes. Possible values: "matter", "holes"
                type="holes",
                // Relevant dimensions of the screws
                screw_diameter=screw_diameter,
                head_diameter=head_diameter,
                // Properties of the ribs
                rib_height = rib_height,
                rib_pitch = vent_opening_pitch,
                // Dimensions of the body shell where the points are located
                shell_length=shell_length,
                shell_width=shell_width,
                shell_height=shell_height,
                shell_wall_thickness = shell_wall_thickness
            );
        }

        // Create the rounded corners.
        body_shell_outer(
            length=shell_length,
            width=shell_width,
            height=shell_height,
            radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );
    }
}
// body_whole();


/* The bottom part of the body */
module body_bottom(
    attachment_points=[[20, 20, "o"], [40, 30, "i"],],
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=5,
    shell_wall_thickness=2
) {
    difference() {
        body_whole(
            attachment_points=attachment_points,
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );

        // Cut the upper part off
        ds = 1;
        translate([- ds/2, - ds/2, partition_height])
        cube([shell_length + ds, shell_width + ds, shell_height + ds]);

    }

    // Add the latch
    body_join_latches(
        enlarged=false,
        partition_height = partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_wall_thickness=shell_wall_thickness
    );
}
// translate([0, 0, -0.1])
// body_bottom();


/* The top part of the body */
module body_top(
    attachment_points=[],
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=5,
    shell_wall_thickness=2
) {
    difference() {
        body_whole(
            attachment_points=attachment_points,
            screw_diameter=screw_diameter,
            head_diameter=head_diameter,
            partition_height = partition_height,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_height=shell_height,
            shell_radius=shell_radius,
            shell_wall_thickness=shell_wall_thickness
        );

        // Cut the lower part off
        ds = 1;
        translate([- ds/2, - ds/2, partition_height - shell_height - ds])
        cube([shell_length + ds, shell_width + ds, shell_height + ds]);

        // Subtract an opening for the latch
        body_join_latches(
            enlarged=true,
            partition_height = partition_height,
            shell_length=shell_length,
            shell_width=shell_width,
            shell_wall_thickness=shell_wall_thickness
        );
    }
}
// body_top();
