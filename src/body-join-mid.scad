/******************************************************************************
    Elements for Joining the Parts in the Middle - Electronics Enclosure
*******************************************************************************

Here are geometric elements for joining the two haves of the body.

This is part of an electronics enclosure.
*/


use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>
// use <body-join-end.scad>
use <end-join.scad>


$fa = 2;
$fs = 0.25;


/* 2D Profile of the seam geometry. */
module body_seam_2D(
    shell_wall_thickness=2.0
) {
    d = shell_wall_thickness;

    polygon([
        [-0.01,     0],
        [2 * d,     d],
        [2 * d, 3 * d],
        [-0.01, 4 * d],
    ]);
}
// body_seam_2D();


/* The seam between the two parts of the body */
module body_seam(
    position="front",
    partition_height = 12,
    shell_length=200,
    shell_width=100,
    shell_wall_thickness=2.0,
) {
    st = shell_wall_thickness;
    el = get_end_length(st);

    tx = position == "rear" ? shell_width : 0;
    mx = position == "rear" ? 1 : 0;

    translate([0, tx, 0])
    mirror([0, mx, 0])

    // The seam itself
    translate([0, 0, partition_height])
    translate([el, st, - 2 * st])
    rotate([90, 0, 90])
    linear_extrude(shell_length - 2 * el)
    body_seam_2D(st);
}
// body_seam(position="rear");


/* The latch that keeps the seam aligned sideways sideways. */
module body_seam_latch(
    enlarged=false,
    position="front",
    head_diameter=5.5,
    partition_height = 12,
    shell_length=200,
    shell_width=100,
    shell_wall_thickness=2.0,
) {
    sl = shell_length;
    st = shell_wall_thickness;
    dl = get_end_length(st) + st * 2 + head_diameter * 1.2;

    // Enlarge the latch by a small size if requested
    de = 0.2 * shell_wall_thickness;

    // Mirror, translate to create rear version
    tx = position == "rear" ? shell_width : 0;
    mx = position == "rear" ? 1 : 0;
    translate([0, tx, 0])
    mirror([0, mx, 0])

    // The latch itself
    translate([0, 0, partition_height])
    translate([dl, 2 * st, 0])
    minkowski() {
    cube([sl - 2*dl, st, st]);
    if (enlarged) {
        cube([de, de, de], center=true);
    }
    }
}
// #body_seam_latch(position="rear");
// body_seam_latch();
// translate([0, 0, 3])
// body_seam_latch(enlarged=true);


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {
        body_shell_outer(shell_wall_thickness=2.0);
        body_shell_inner(shell_wall_thickness=2.0);
    }

    %body_seam(position="front");
    %body_seam(position="rear");

    #body_seam_latch(position="front");
    #body_seam_latch(position="rear");
}
test_geometry();
