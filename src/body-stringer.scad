/******************************************************************************
                        Stringers - Electronics Enclosure
*******************************************************************************

This file contains stringers, reinforcing structures that run lengthwise in the
body.

This is part of an electronics enclosure.
*/


use <helpers.scad>
use <boxes.scad>
use <base-geometry.scad>


$fa = 2;
$fs = 0.25;


/* The global spacing of all stringers. */
function get_stringer_pitch() = 15;


/* The basic part of a stringer.

Has the correct length, and the correct lengthwise position.
*/
module stringer_base(
    width=3,
    height=5,
    end_dist = 10,
    shell_length=200,
) {
    ds = 0.02;
    stringer_length = shell_length - 2 * end_dist + ds;

    translate([(shell_length - stringer_length) / 2, 0, 0])
    cube([stringer_length, width, height + ds]);
}
// stringer_base();


/* Creates a series of stringers on the top or bottom of the enclosure */
module stringer_top_bottom(
    // Parameters of each stringer
    width=3,
    height=5,
    // spacing of the stringers
    pitch=15,
    // Where should the stringer be placed: "top" or "bottom"
    position="bottom",
    // Parameters of the enclosure body
    end_dist = 10,
    partition_height=12, // unused
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    tz = position == "top"
       ? shell_height - shell_wall_thickness - height + 0.01
       : shell_wall_thickness - 0.01;

    translate([0, 0, tz]) {
        // Place stringers evenly on top or bottom
        pos = regular_pattern_centered(
            length=shell_width - 2 * shell_radius,
            pitch=pitch
        );
        dy = shell_radius - width / 2;

        for(y=pos) {
            translate([0, y + dy, 0])
            stringer_base(
                width=width, height=height,
                end_dist = end_dist, shell_length=shell_length
            );
        }
    }
}
// stringer_top_bottom(position="top");
// stringer_top_bottom(position="bottom");


/* Creates a series of stringers on the sides of the enclosure */
module stringer_sides(
    // Parameters of each stringer
    width=3,
    height=5,
    // spacing of the stringers
    pitch=15,
    // Where should the stringer be placed: "front" or "rear"
    position="front",
    // Parameters of the enclosure body
    end_dist = 10,
    partition_height=12,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    ty = position == "rear"
       ? shell_width - shell_wall_thickness - height + 0.01
       : shell_wall_thickness - 0.01;

    translate([0, ty, 0]) {
        // Place stringers evenly on the sides
        pos = regular_pattern_centered(
            length=shell_height - shell_radius - partition_height,
            pitch=pitch
        );
        dz = partition_height - width / 2;

        for(z=pos) {
            translate([0, 0, z + dz])
            translate([0, height, 0])
            rotate([90, 0, 0])
            stringer_base(
                width=width, height=height,
                end_dist = end_dist, shell_length=shell_length
            );
        }
    }
}
// stringer_sides(position="front");
// // stringer_sides(position="rear");
// translate([0, 0, 12])
// square([200, 100]);


/* All stringers of the body */
module stringer_all(
    // Parameters of each stringer
    width=3,
    height=5,
    // spacing of the stringers
    pitch=15,
    // Parameters of the enclosure body
    end_dist = 10,
    partition_height=12,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    stringer_top_bottom(
        width=width,
        height=height,
        pitch=pitch,
        position="top",
        end_dist = end_dist,
        partition_height=partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
    stringer_top_bottom(
        width=width,
        height=height,
        pitch=pitch,
        position="bottom",
        end_dist = end_dist,
        partition_height=partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
    stringer_sides(
        width=width,
        height=height,
        pitch=pitch,
        position="front",
        end_dist = end_dist,
        partition_height=partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
    stringer_sides(
        width=width,
        height=height,
        pitch=pitch,
        position="rear",
        end_dist = end_dist,
        partition_height=partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
}
// stringer_all();


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {
        body_shell_outer();
        body_shell_inner();
    }

    stringer_all();
}
test_geometry();
