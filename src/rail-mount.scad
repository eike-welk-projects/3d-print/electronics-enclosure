/******************************************************************************
                Rail Mount - Electronics Enclosure
*******************************************************************************

This file contains a rail mount to fix the enclosure to an other object, or
piece of furniture. The rail mount is intended for tight spaces, and to hide the
screws. A much simpler alternative are the Screw on Feet.

This is part of an electronics enclosure.
*/


use <helpers.scad>
use <boxes.scad>
use <base-geometry.scad>


$fa = 2;
$fs = 0.25;


/* Through hole for screw, with sunk head. */
module hole_screw_head(
    screw_diameter = 2.9,
    head_diameter = 5.5,
) {
    ds = 0.02;

    head_through_d = screw_head_through_diameter(head_diameter);
    screw_through_d = screw_thread_through_diameter(screw_diameter);
    // screw_bite_d = screw_bite_diameter(screw_diameter);

    conus_h = (head_through_d - screw_through_d) / 2;

    screw_through_h = 20;
    shell_height = 50;

    translate([0, 0, -screw_through_h]) {
        // Upper part of the hole, which is wide enough for the head of the
        // screw to pass through.
        translate([0, 0, screw_through_h + conus_h - ds/2])
        cylinder(
            d=head_through_d,
            h=shell_height - screw_through_h - conus_h + ds
        );

        // Middle part were screw goes through. Its wall conducts the clamping
        // force of the screw.
        // Conical part
        translate([0, 0, screw_through_h - ds/2])
        cylinder(d1=screw_through_d, d2=head_through_d, h=conus_h + ds);
        // cylindrical though hole
        translate([0, 0, ds/2])
        cylinder(d=screw_through_d, h=screw_through_h);

        // The bottom cylindical hole, where the screw bites into, is in a separate
        // part.
    }
}
// hole_screw_head();


/* The 2D cross section of the rail. */
module rail_shape_2D(
    rail_width=30,
    rail_height=6
) {
    head_h = 1;
    foot_h = 2;
    slant_h = rail_height - head_h - foot_h;

    polygon([
        [0,                     0                           ],
        [rail_width,            0                           ],
        [rail_width,            head_h                      ],
        [rail_width - slant_h,  head_h + slant_h            ],
        [rail_width - slant_h,  head_h + slant_h + foot_h   ],
        [slant_h,               head_h + slant_h + foot_h   ],
        [slant_h,               head_h + slant_h            ],
        [0,                     head_h                      ],
    ]);
}
// rail_shape_2D();


/* The mounting rail itself. */
module rail_mount_rail(
    // dimensions of the screws
    screw_diameter = 2.9,
    head_diameter = 5.5,
    // dimension of the rail itself
    rail_width=23,
    rail_height = 6,
    rail_length = 130,
) {
    ds = 0.02;

    end_dist = 10;
    hole_dist = 20;
    bottom_thickness = 1.5;

    screw_bite_d = screw_bite_diameter(screw_diameter);

    translate([-rail_width/2, 0, 0])
    difference() {
        // The rail itself
        translate([0, 0, rail_height])
        rotate([-90, 0, 0])
        linear_extrude(height=rail_length)
        rail_shape_2D(
            rail_width=rail_width,
            rail_height=rail_height
        );

        // The through holes for the fastening screws
        hole_pos = regular_pattern_centered(
            length=rail_length - 2 * end_dist,
            pitch=hole_dist
        );
        translate([rail_width/2, end_dist, 0])
        for (y = hole_pos) {
            translate([0, y, bottom_thickness])
            hole_screw_head(
                screw_diameter=screw_diameter,
                head_diameter=head_diameter
            );
        }

        // The holes for the locking screw
        translate_copy([0, rail_length - 15 + 2*ds, 0])
        translate([rail_width/2, -ds, rail_height/2])
        rotate([-90, 0, 0])
        cylinder(d=screw_bite_d, h=15);
    }
}
// rail_mount_rail();


/* The feet for the body, that slide along the rail. */
module rail_mount_feet(
    // Integrate an end stop with a locking screw for the front feet.
    with_end_stop=false,
    // Dimensions of the screws
    screw_diameter = 2.9,
    head_diameter = 5.5,
    // Dimension of the rail
    rail_width=23,
    rail_height = 6,
    // Distance between the mounting holes for the feet.
    feet_hole_width=30,
    // Radius of the rounded edges.
    edge_radius=4,
    // Thickness of the walls. Also thickness of ribs and stringers.
    shell_wall_thickness=1.2,
) {
    ds = 0.02;

    // Clearance from the mounting surface where the enclosure is screwed to.
    clearance = 0.5;
    // Thickness of the connection between the two feet.
    strip_thickness = 0.5;
    // Thickness of the end stop
    end_stop_thickness = 2 * shell_wall_thickness;

    // The space for the rail needs to be slightly wider than the rail itself.
    rail_width_ex = rail_width * 1.03;

    head_through_d = screw_head_through_diameter(head_diameter);
    screw_through_d = screw_thread_through_diameter(screw_diameter);

    // Dimensions of outer shape
    feet_width = feet_hole_width + head_through_d + 4*shell_wall_thickness;
    feet_length = head_through_d + 4*shell_wall_thickness;
    feet_height = rail_height + strip_thickness - clearance;

    translate([-feet_width/2, 0, 0])
    difference() {
        // Outer shape
        roundedCube(
            [feet_width, feet_length, feet_height],
            r=edge_radius,
            sidesonly=true,
            center=false
        );

        // Cut out space for the rail
        // If required, leave a vertical wall as an end stop.
        ty_end_stop = with_end_stop
                    ? end_stop_thickness + ds/2
                    : 0;
        translate([0, ty_end_stop, 0]) // Move the cut, so that the end stop remains
        translate([
            feet_width/2 - rail_width_ex/2,
            -ds/2,
            rail_height - clearance
        ])
        rotate([-90, 0, 0])
        linear_extrude(height=feet_length + ds)
        rail_shape_2D(
            rail_width=rail_width_ex,
            rail_height=rail_height
        );

        // Through hole for the locking screw (in the end stop);
        if (with_end_stop) {
            translate([
                feet_width/2,
                -ds/2,
                rail_height/2 - clearance
            ])
            rotate([-90, 0, 0])
            cylinder(d=screw_through_d, h=end_stop_thickness + ds);
        }

        // Mounting holes for the feet
        translate_copy([feet_hole_width, 0, 0])
        translate([
            feet_width/2 - feet_hole_width/2,
            feet_length/2,
            feet_height - shell_wall_thickness
        ])
        rotate([180, 0, 0])
        hole_screw_head(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter
        );
    }
}
// rail_mount_feet();
// rail_mount_feet(with_end_stop=true);


// // Test geometry ---
// rail_mount_feet(with_end_stop=false);
// // rail_mount_feet(with_end_stop=true);
// translate([0, 1.5, -0.5])
// #rail_mount_rail();


// Print geometry
rail_mount_rail();

translate([15, 21, 6.0])
rotate([180, 0, 90])
rail_mount_feet();

translate([30, 21, 6.0])
rotate([180, 0, 90])
rail_mount_feet(with_end_stop=true);
