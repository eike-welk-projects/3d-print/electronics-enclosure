/******************************************************************************
                Cable Strain Relief - Electronics Enclosure
*******************************************************************************

This file contains a strain relief for cables that exit the enclosure.
The cables are securely attached to the lower body with this group of
components.

This is part of an electronics enclosure.
*/


// use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>


$fa = 2;
$fs = 0.25;


/* A rectangular hole */
module cable_hole_rect(
    size_y=20,
    size_z=10,
    plate_thickness=3
) {
    size_x = 3 * plate_thickness;
    cube([size_x, size_y, size_z], center=true);
}
// cable_hole_rect();


/* A round hole which can be elongated

Parameters
----------

size_y
    The size in Y direction

    The smaller size is used as the radius, the larger value is used as the
    width of the elongated hole.

    A for a perfectly circular hole `size_y` and `size_z` mus be the same.

size_z
    The size in Z direction

plate_thickness
    The thickness of the end plate.
*/
module cable_hole_round(
    size_y=10,
    size_z=20,
    plate_thickness=3
) {
    diameter = min(size_y, size_z);
    width = max(size_y, size_z);
    dist = width - diameter;
    // Orientation: vertical or horizontal
    rx = size_y < size_z ? 90 : 0;

    height = 3 * plate_thickness;

    rotate([rx, 0, 0])
    rotate([0, 90, 0])
    if (width > diameter) {
        hull() {
            translate([0, dist/2, 0])
            cylinder(d=diameter, h=height, center=true);
            translate([0, -dist/2, 0])
            cylinder(d=diameter, h=height, center=true);
        }
    } else {
        cylinder(d=diameter, h=height, center=true);
    }
}
// cable_hole_round();


/* Generate holes for the end plates

Parameters
----------

holes: [[...], ...]
    Parameters for the holes in the end plate.

    * The dimension are in the Y-Z-plane, the origin is the global origin.
    * The first value is the position of the hole's center in Y-direction.
    * The 2nd value is the type "round" or "rect" (rectangular).
    * The 3rd and 4th values are the size in Y- and Z- direction.

    For example:
    [
        [pos_y, "rect", size_y, size_z]
        // elongated round hole: smaller value is diameter, larger value is length
        [pos_y, "round", size_y, size_z]
    ]

plate_thickness: number
    The thickness of the plate where the hole(s) go through.
*/
module cable_hole_arrangement(
    // The attachment points
    holes=[
        [50, "rect" , 20, 10],
        [30, "round",  5,  5],
        [70, "round", 15,  5],
    ],
    // Properties of the ribs
    plate_thickness = 3,
) {
    // Generate the attachment points
    for (i = [0 : 1 : len(holes) - 1]) {
        pt = holes[i];
        pos_y = pt[0];
        shape = pt[1];

        translate([0, pos_y, 0])
        if (shape == "rect") {
            cable_hole_rect(
                size_y=pt[2],
                size_z=pt[3],
                plate_thickness=plate_thickness
            );
        } else {
            cable_hole_round(
                size_y=pt[2],
                size_z=pt[3],
                plate_thickness=plate_thickness
            );
        }
    }
}
// cable_hole_arrangement();


/* A simple strain relief

Two identical copies of this module create a simple strain relief for cables.
This strain relief can clamp multiple cables.

Parameters
----------

screw_distance: number
    Distance between the screws that fix the strain relief to the body.
    The cables run between the screws.

screw_diameter: number
    Nominal (outer) diameter of the screws, that fix the strain relief to the
    body.

head_diameter:
    Head diameter of the screws, that fix the strain relief to the body.

holes: [[...], ...]
    Parameters for the holes in the end plate.

    The holes are somewhat shrunk in the Z-direction, so that they clamp the
    cables. Therefore the true sizes of the cables can be given.

    * The dimension are in the Y-Z-plane. The origin is the center of strain
      relief, which is on the upper surface of the module, in the middle between
      the screws.
    * The first value is the position of the hole's center in Y-direction.
    * The 2nd value is the type "round" or "rect" (rectangular).
    * The 3rd and 4th values are the size in Y- and Z- direction.

    For example:
    [
        [pos_y, "rect", size_y, size_z]
        // elongated round hole: smaller value is diameter, larger value is length
        [pos_y, "round", size_y, size_z]
    ]

plate_thickness: number
    The thickness of the plate where the hole(s) go through.
*/
module strain_relief_simple(
    // Screws to fix the strain relief to the body
    screw_distance=30,
    // Relevant dimensions of the screws
    screw_diameter=2.9,
    head_diameter=5.5,
    // Holes
    holes=[
        [-5, "round", 6, 4],
        [ 5, "round", 6, 4],
    ],
) {
    ds = 0.02;
    clamp_shrink = 0.80;

    // The general shape
    height = screw_head_through_diameter(head_diameter) * 0.75;
    width = screw_head_through_diameter(head_diameter);

    difference() {
        hull() {
            cylinder(d=width, h=height);
            translate([0, screw_distance, 0])
            cylinder(d=width, h=height);
        }

        // screw holes
        through_d =  screw_thread_through_diameter(screw_diameter);

        translate([0, 0 , -ds/2])
        cylinder(d=through_d, h=height + ds);
        translate([0, screw_distance, -ds/2])
        cylinder(d=through_d, h=height + ds);

        translate([0, screw_distance/2, height])
        scale([1, 1, clamp_shrink])
        cable_hole_arrangement(holes, width);
    }
}
strain_relief_simple();

translate([0, 0, 12])
rotate([0, 180, 0])
strain_relief_simple();
