/******************************************************************************
          Parts for Printing - Electronics Enclosure for BT-5.0-Audio
*******************************************************************************

This file contains all the necessary parts of an electronics enclosure, arranged
for printing.

To render this file with OpenScad, takes several minutes. Be patient.

The enclosure is configured as a housing for the bluetooth audio receiver
BT-5.0-Audio. Additionally it contains a step-down power converter. There is a
power switch is on the front of the enclosure. Connectors for DC power
(7V - 30V) and audio output are on the back.

The bluetooth receiver is mounted high in the enclosure, on a separate mounting
plate.

Additionally to the printed parts you need:
* Bluetooth receiver "BT-5.0 Audio"
  https://www.aliexpress.com/item/1005002231624213.html
* Step down power converter "LM2596 HW-411"
  https://www.aliexpress.com/item/32653212622.html
* Power switch for montage in rectangular hole: 19 x 13 mm
  https://www.aliexpress.com/i/32619607524.html
* Socket for a barrel jack: Mounting thread diameter 7.6 mm, two flat sides with
  distance of 6.6 mm.
  https://www.aliexpress.com/item/32883658107.html
*/


use <helpers.scad>
use <base-geometry.scad>
use <body.scad>
use <end-plate-flat.scad>
use <end-plate-curvy.scad>
use <cable-strain-relief.scad>
use <rail-mount.scad>
use <screw-on-feet.scad>


$fa = 4;
$fs = 0.25;


// Properties of the screws
// The screws, that hold the parts together, are self tapping screws. There are
// no nuts.
// Diameter of the screws.
screw_diameter = 2.9;
// Diameters of the screw heads.
head_diameter = 5.5;

// Shape of the body
// Height where the top and bottom halves are split apart.
partition_height = 12;
// Length of the body (without the ends).
shell_length = 105;
// Width of the body
shell_width = 50;
// Height of the body
shell_height = 30; // 50;
// Radius at the edges of the body
shell_radius = 5;
// Thickness of the walls. Also thickness of ribs and stringers.
shell_wall_thickness = 1.2;

// Feet and mounting rail
// Distance between the mounting holes for the feet. (In width direction.)
feet_hole_width = shell_width - 20;

// Points where components are screwed to the bottom half of the body.
//
// These are small cylinders a with hole for a screw. The properties of the
// screw are `screw_diameter`, `head_diameter`.
//
// The values are:
// * Location X-coordinate (length direction)
// * Location Y-coordinate (width direction)
// * Type "inner" (tall cylinder, to screw something to the inside) or
//        "outer" (to screw something to the outside)
attachment_points=[
    // Feet
    [7,                 shell_width/2 - feet_hole_width/2,      "outer"],
    [7,                 shell_width/2 + feet_hole_width/2,      "outer"],
    [shell_length - 7,  shell_width/2 - feet_hole_width/2,      "outer"],
    [shell_length - 7,  shell_width/2 + feet_hole_width/2,      "outer"],
    // Mounting plate for BT 5.0
    [14,                17.5,                                     "inner"],
    [35,                17.5,                                     "inner"],
    // Transformer
    [48,                shell_width/2 + 21.5/2 - 2.5,                    "inner"],
    [48 + 30.5,         shell_width/2 - 21.5/2 + 2.5,                    "inner"],
];


// Body -----------------------------------------------------------------------
// Bottom half of the body
body_bottom(
    attachment_points=attachment_points,
    screw_diameter=screw_diameter,
    head_diameter=head_diameter,
    partition_height = partition_height,
    shell_length=shell_length,
    shell_width=shell_width,
    shell_height=shell_height,
    shell_radius=shell_radius,
    shell_wall_thickness=shell_wall_thickness
);

// Top half of the body
translate([0, 2 * shell_width + 5, shell_height])
rotate([180, 0, 0])
body_top(
    attachment_points=[], // currently ignored
    screw_diameter=screw_diameter,
    head_diameter=head_diameter,
    partition_height = partition_height,
    shell_length=shell_length,
    shell_width=shell_width,
    shell_height=shell_height,
    shell_radius=shell_radius,
    shell_wall_thickness=shell_wall_thickness
);


// End Plates -----------------------------------------------------------------
// A flat end plate
audio_plug_y = 37.2 - 5/2;
audio_plug_z = 25.0 - 5/2;
audio_plug_d =  5.5;
rib_height = get_rib_height(shell_width, shell_height);

translate([-10 - shell_height, 0, rib_height])
rotate([0, -90, 0])
difference() {
    end_plate_flat(
        // Configuration for the holes in the end plate.
        // The values are:
        // * Location (center) Y-coordinate (width direction)
        //   The origin is in the bottom right corner for a plate in "left" position,
        //   and the bottom left corner for a plate in "right" position.
        // * Location (center) Z-coordinate (height direction)
        // * Type "round"/"rect"
        // * Size Y (width direction)
        // * Size Z (height direction)
        holes=[
            // Hole for audio output
            [audio_plug_y,      audio_plug_z,   "round",    audio_plug_d,   audio_plug_d],
            // Hole for USB-C connector - hidden
            [22.5 - 7.7/2,      22.1 - 2.7/2,   "round",    9,              4],
            // Hole for power connector
            [shell_width/2 + 7, 4.5 + 12.4/2,   "round",    8.5,            7.0],
        ],
        position = "left",
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );

    // Reduce thickness of end plate around audio outlet connector.
    translate([-1.2, audio_plug_y, audio_plug_z])
    rotate([0, -90, 0])
    cylinder(d=10, h=3);
}

// A curvy end plate
translate([-5 - shell_height, 0, shell_radius + shell_length])
rotate([0, 90, 0])
end_plate_curvy(
    // Configuration for the holes in the end plate.
    // The values are:
    // * Location (center) Y-coordinate (width direction)
    //   The origin is in the bottom right corner for a plate in "left" position,
    //   and the bottom left corner for a plate in "right" position.
    // * Location (center) Z-coordinate (height direction)
    // * Type "round"/"rect"
    // * Size Y (width direction)
    // * Size Z (height direction)
    holes=[
        // Rectangular hole for a switch
        [shell_width/2, shell_height/2, "rect", 19, 13]
    ],
    position = "right",
    shell_length=shell_length,
    shell_width=shell_width,
    shell_height=shell_height,
    shell_radius=shell_radius,
    shell_wall_thickness=shell_wall_thickness
);


// Mounting plate for BT 5.0 -------------------
module mounting_plate_bt50(
    height=10,
    thickness=2,
) {
    ds = 0.02;

    difference() {
        // Bottom plate
        cube([32, 18, thickness]);

        // Mounting holes plate-body
        translate_copy(t=[21, 0, 0], n=2)
        translate([7.5, 7.5, 0])
        translate([0, 0, -ds/2])
        cylinder(d=3.5, thickness + ds);
    }

    // Mounting spacers for BT 5.0
    translate_copy([19.5, -12.5, 0])
    translate([2.5, 15.0, 0])
    difference() {
        cylinder(d=5, h=height);

        translate([0, 0, -ds/2])
        cylinder(d=1.6, h=height + ds);
    }
}
// translate([6.5, 10.0, 8.5]) // Put mounting plate, where it is used.
translate([0, -25, 0])
mounting_plate_bt50();
