/******************************************************************************
                Parts for Printing - Electronics Enclosure
*******************************************************************************

This file contains all the necessary parts of the electronics enclosure,
arranged for printing.

To render this file with OpenScad, takes several minutes. Be patient.

The enclosure is configured as a power supply for a short LED strip. It contains
a "MEAN WELL APV-8-12" power supply and a mains switch. The switch is on the
front of the enclosure, the cables exit on the back.

To create your own electronics enclosure you should copy this file, and adapt
the various configuration values to your needs.
*/


use <helpers.scad>
use <base-geometry.scad>
use <body.scad>
use <end-plate-flat.scad>
use <end-plate-curvy.scad>
use <cable-strain-relief.scad>
use <rail-mount.scad>
use <screw-on-feet.scad>


$fa = 4;
$fs = 0.25;


// Properties of the screws
// The screws, that hold the parts together, are self tapping screws. There are
// no nuts.
// Diameter of the screws.
screw_diameter = 2.9;
// Diameters of the screw heads.
head_diameter = 5.5;

// Shape of the body
// Height where the top and bottom halves are split apart.
partition_height = 12;
// Length of the body (without the ends).
shell_length = 130;
// Width of the body
shell_width = 50;
// Height of the body
shell_height = 40; // 50;
// Radius at the edges of the body
shell_radius = 5;
// Thickness of the walls. Also thickness of ribs and stringers.
shell_wall_thickness = 1.2;

// Feet and mounting rail
// Distance between the mounting holes for the feet. (In width direction.)
feet_hole_width = shell_width - 20;
// width of the mounting rail
mounting_rail_width=23;
// Height of the mounting rail
mounting_rail_height = 6;

// Width of the cable strain relief
strain_relief_width = 30;

// Points where components are screwed to the bottom half of the body.
//
// These are small cylinders a with hole for a screw. The properties of the
// screw are `screw_diameter`, `head_diameter`.
//
// The values are:
// * Location X-coordinate (length direction)
// * Location Y-coordinate (width direction)
// * Type "inner" (tall cylinder, to screw something to the inside) or
//        "outer" (to screw something to the outside)
attachment_points=[
    // Feet
    [7,                 shell_width/2 - feet_hole_width/2,      "outer"],
    [7,                 shell_width/2 + feet_hole_width/2,      "outer"],
    [shell_length - 7,  shell_width/2 - feet_hole_width/2,      "outer"],
    [shell_length - 7,  shell_width/2 + feet_hole_width/2,      "outer"],
    // Cable strain relief
    [14,                shell_width/2 - strain_relief_width/2,  "inner"],
    [14,                shell_width/2 + strain_relief_width/2,  "inner"],
    // Transformer
    [33,                shell_width/2 + 4.5,                    "inner"],
    [33 + 66,           shell_width/2 - 4.5,                    "inner"],
];


// Body -----------------------------------------------------------------------
// Bottom half of the body
body_bottom(
    attachment_points=attachment_points,
    screw_diameter=screw_diameter,
    head_diameter=head_diameter,
    partition_height = partition_height,
    shell_length=shell_length,
    shell_width=shell_width,
    shell_height=shell_height,
    shell_radius=shell_radius,
    shell_wall_thickness=shell_wall_thickness
);

// Top half of the body
translate([0, 2 * shell_width + 5, shell_height])
rotate([180, 0, 0])
body_top(
    attachment_points=[], // currently ignored
    screw_diameter=screw_diameter,
    head_diameter=head_diameter,
    partition_height = partition_height,
    shell_length=shell_length,
    shell_width=shell_width,
    shell_height=shell_height,
    shell_radius=shell_radius,
    shell_wall_thickness=shell_wall_thickness
);

// End Plates -----------------------------------------------------------------
// A flat end plate
rib_height = get_rib_height(shell_width, shell_height);
translate([-10 - shell_height, 0, rib_height])
rotate([0, -90, 0])
end_plate_flat(
    // Configuration for the holes in the end plate.
    // The values are:
    // * Location (center) Y-coordinate (width direction)
    //   The origin is in the bottom right corner for a plate in "left" position,
    //   and the bottom left corner for a plate in "right" position.
    // * Location (center) Z-coordinate (height direction)
    // * Type "round"/"rect"
    // * Size Y (width direction)
    // * Size Z (height direction)
    holes=[
        [shell_width/2 - 5, shell_height*0.3, "round", 4.0, 3.1],
        [shell_width/2 + 5, shell_height*0.3, "round", 6.1, 3.6],
    ],
    position = "left",
    shell_length=shell_length,
    shell_width=shell_width,
    shell_height=shell_height,
    shell_radius=shell_radius,
    shell_wall_thickness=shell_wall_thickness
);

// A curvy end plate
translate([-5 - shell_height, 0, shell_radius + shell_length])
rotate([0, 90, 0])
end_plate_curvy(
    // Configuration for the holes in the end plate.
    // The values are:
    // * Location (center) Y-coordinate (width direction)
    //   The origin is in the bottom right corner for a plate in "left" position,
    //   and the bottom left corner for a plate in "right" position.
    // * Location (center) Z-coordinate (height direction)
    // * Type "round"/"rect"
    // * Size Y (width direction)
    // * Size Z (height direction)
    holes=[[shell_width/2, shell_height/2, "rect", 19, 13]],
    position = "right",
    shell_length=shell_length,
    shell_width=shell_width,
    shell_height=shell_height,
    shell_radius=shell_radius,
    shell_wall_thickness=shell_wall_thickness
);

// Cable strain relief --------------------------------------------------------
translate([-10, shell_width + 10, 0])
translate_copy([-10, 0, 0])
strain_relief_simple(
    // Screws to fix the strain relief to the body
    screw_distance=30,
    // Relevant dimensions of the screws
    screw_diameter=screw_diameter,
    head_diameter=head_diameter,
    // Configuration for the holes in the end plate.
    // The values are:
    // * Location (center) Y-coordinate (width direction).
    //   The origin is in the middle!
    // * Type "round"/"rect"
    // * Size Y (width direction)
    // * Size Z (height direction)
    holes=[
        [-5, "round", 4.0, 2.0],
        [ 5, "round", 6.1, 3.6],
    ]
);

// Rail mount ------------------------------------------------------------------
// Mounting rail
translate([0, 2*shell_width + mounting_rail_width/2 + 10, 0])
rotate([0, 0, -90])
rail_mount_rail(
    // Dimensions of the screws
    screw_diameter=screw_diameter,
    head_diameter=head_diameter,
    // dimension of the rail itself
    rail_width=mounting_rail_width,
    rail_height=mounting_rail_height,
    rail_length=shell_length
);

// Rear foot, can freely slide along the rail
translate([-15, 2*shell_width + mounting_rail_width/2 + 10, mounting_rail_height])
rotate([180, 0, 90])
rail_mount_feet(
    // Integrate an end stop with a locking screw for the front feet.
    with_end_stop=false,
    // Dimensions of the screws
    screw_diameter=screw_diameter,
    head_diameter=head_diameter,
    // Dimension of the rail
    rail_width=mounting_rail_width,
    rail_height=mounting_rail_height,
    // Distance between the mounting holes for the feet.
    feet_hole_width=feet_hole_width,
    // Radius of the rounded edges.
    edge_radius=shell_radius,
    // Thickness of the walls. Also thickness of ribs and stringers.
    shell_wall_thickness=shell_wall_thickness
);

// Front foot, with end stop
translate([-30, 2*shell_width + mounting_rail_width/2 + 10, mounting_rail_height])
rotate([180, 0, 90])
rail_mount_feet(
    // Integrate an end stop with a locking screw for the front feet.
    with_end_stop=true,
    // Dimensions of the screws
    screw_diameter=screw_diameter,
    head_diameter=head_diameter,
    // Dimension of the rail
    rail_width=mounting_rail_width,
    rail_height=mounting_rail_height,
    // Distance between the mounting holes for the feet.
    feet_hole_width=feet_hole_width,
    // Radius of the rounded edges.
    edge_radius=shell_radius,
    // Thickness of the walls. Also thickness of ribs and stringers.
    shell_wall_thickness=shell_wall_thickness
);

// // Screw on Feet --------------------------------------------------------------
// translate_copy([-15, 0, 0]) // Create two identical feet
// translate([-45, shell_width + 5, 0])
// // translate([0, -11, 0])
// screw_on_feet(
//     // Dimensions of the screws
//     screw_diameter = screw_diameter,
//     head_diameter = head_diameter,
//     // Distance between the mounting holes in the body.
//     feet_hole_width=feet_hole_width,
//     // Distance between the mounting holes in the surface. (Outer holes)
//     surface_hole_width=shell_width + 10,
//     // Height of the feet
//     feet_height=10,
//     // Radius of the rounded edges.
//     edge_radius=4,
//     // Thickness of the walls of the body. Also thickness of ribs and stringers.
//     shell_wall_thickness=shell_wall_thickness
// );
