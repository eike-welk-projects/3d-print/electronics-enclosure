/******************************************************************************
                    Screw on Feet - Electronics Enclosure
*******************************************************************************

This file contains simple feet for the enclosure to screw the it to a surface.
These feet can be used, when space and optics are not a concern. If space is
constrained or when the screws should be hidden, consider the Rail Mount.

This is part of an electronics enclosure.
*/


use <helpers.scad>
use <boxes.scad>
use <base-geometry.scad>


$fa = 2;
$fs = 0.25;


/* Through hole for screw, with sunk head. */
module hole_screw_head_2(
    screw_diameter = 2.9,
    head_diameter = 5.5,
) {
    ds = 0.02;

    head_through_d = screw_head_through_diameter(head_diameter);
    screw_through_d = screw_thread_through_diameter(screw_diameter);
    // screw_bite_d = screw_bite_diameter(screw_diameter);

    conus_h = (head_through_d - screw_through_d) / 2;

    screw_through_h = 20;
    shell_height = 50;

    translate([0, 0, -screw_through_h]) {
        // Upper part of the hole, which is wide enough for the head of the
        // screw to pass through.
        translate([0, 0, screw_through_h + conus_h - ds/2])
        cylinder(
            d=head_through_d,
            h=shell_height - screw_through_h - conus_h + ds
        );

        // Middle part were screw goes through. Its wall conducts the clamping
        // force of the screw.
        // Conical part
        translate([0, 0, screw_through_h - ds/2])
        cylinder(d1=screw_through_d, d2=head_through_d, h=conus_h + ds);
        // cylindrical though hole
        translate([0, 0, ds/2])
        cylinder(d=screw_through_d, h=screw_through_h);

        // The bottom cylindrical hole, where the screw bites into, is in a separate
        // part.
    }
}
// hole_screw_head();


/* Simple feet for the body, that are screwed to a surface. */
module screw_on_feet(
    // Dimensions of the screws
    screw_diameter = 2.9,
    head_diameter = 5.5,
    // Distance between the mounting holes in the body.
    feet_hole_width=30,
    // Distance between the mounting holes in the surface. (Outer holes)
    surface_hole_width=50,
    // Height of the feet
    feet_height=10,
    // Radius of the rounded edges.
    edge_radius=4,
    // Thickness of the walls of the body. Also thickness of ribs and stringers.
    shell_wall_thickness=1.2
) {
    ds = 0.02;

    head_through_d = screw_head_through_diameter(head_diameter);
    screw_through_d = screw_thread_through_diameter(screw_diameter);

    // Dimensions of outer shape
    feet_width = surface_hole_width + head_through_d + 4*shell_wall_thickness;
    feet_length = head_through_d + 4*shell_wall_thickness;

    // translate([-feet_length/2, -feet_width/2, 0])
    difference() {
        // Outer shape
        roundedCube(
            [feet_length, feet_width, feet_height],
            r=edge_radius,
            sidesonly=true,
            center=false
        );

        // Holes for mounting feet to the body
        translate_copy([0, feet_hole_width, 0])
        translate([feet_length/2, feet_width/2 - feet_hole_width/2, feet_height - shell_wall_thickness])
        rotate([180, 0, 0])
        hole_screw_head_2(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter
        );

        // Holes for mounting feet to the surface
        translate_copy([0, surface_hole_width, 0])
        translate([feet_length/2, feet_width/2 - surface_hole_width/2, shell_wall_thickness])
        hole_screw_head_2(
            screw_diameter=screw_diameter,
            head_diameter=head_diameter
        );
    }
}
screw_on_feet();
