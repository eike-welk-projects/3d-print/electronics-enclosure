/******************************************************************************
            Holes for End Plates - Electronics Enclosure
*******************************************************************************

This file contains the definitions for holes in end plates. These holes are for
switches LEDs and cables.

This is part of an electronics enclosure.
*/


// use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>
// use <body-join-end.scad>
// use <end-join.scad>
// use <body-stringer.scad>


$fa = 2;
$fs = 0.25;


/* A rectangular hole */
module end_hole_rect(
    size_y=20,
    size_z=10,
    plate_thickness=3
) {
    size_x = 3 * plate_thickness;
    cube([size_x, size_y, size_z], center=true);
}
// end_hole_rect();


/* A round hole which can be elongated

Parameters
----------

size_y
    The size in Y direction

    The smaller size is used as the radius, the larger value is used as the
    width of the elongated hole.

    A for a perfectly circular hole `size_y` and `size_z` mus be the same.

size_z
    The size in Z direction

plate_thickness
    The thickness of the end plate.
*/
module end_hole_round(
    size_y=10,
    size_z=20,
    plate_thickness=3
) {
    diameter = min(size_y, size_z);
    width = max(size_y, size_z);
    dist = width - diameter;
    // Orientation: vertical or horizontal
    rx = size_y < size_z ? 90 : 0;

    height = 3 * plate_thickness;

    rotate([rx, 0, 0])
    rotate([0, 90, 0])
    if (width > diameter) {
        hull() {
            translate([0, dist/2, 0])
            cylinder(d=diameter, h=height, center=true);
            translate([0, -dist/2, 0])
            cylinder(d=diameter, h=height, center=true);
        }
    } else {
        cylinder(d=diameter, h=height, center=true);
    }
}
// end_hole_round();


/* Generate holes for the end plates

Parameters
----------

holes: [[...], ...]
    Parameters for the holes in the end plate.

    * The dimension are in the Y-Z-plane, the origin is the global origin.
    * The first two values are the position of the hole's center.
    * The 3rd value is the type "round" or "rect" (rectangular).
    * The 4th and 5th values are the size in Y- and Z- direction.

    For example:
        [[pos_y, pos_z, "rect", size_y, size_z]]

plate_thickness: number
    The thickness of the end plate.
*/
module end_hole_arrangement(
    // The attachment points
    holes=[
        [50, 30, "rect" , 20, 10],
        [30, 10, "round",  5,  5],
        [70, 10, "round", 15,  5],
    ],
    // Properties of the ribs
    plate_thickness = 3,
) {
    // Generate the attachment points
    for (i = [0 : 1 : len(holes) - 1]) {
        pt = holes[i];
        pos_y = pt[0];
        pos_z = pt[1];
        shape = pt[2];

        translate([0, pos_y, pos_z])
        if (shape == "rect") {
            end_hole_rect(
                size_y=pt[3],
                size_z=pt[4],
                plate_thickness=plate_thickness
            );
        } else {
            end_hole_round(
                size_y=pt[3],
                size_z=pt[4],
                plate_thickness=plate_thickness
            );
        }
    }
}
end_hole_arrangement();


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {
        body_shell_outer(shell_wall_thickness=2);
        body_shell_inner(shell_wall_thickness=2);
    }

}
test_geometry();
