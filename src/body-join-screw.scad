/******************************************************************************
       Geometry to Screw the Body Together - Electronics Enclosure
*******************************************************************************

This file contains geometric elements to screw the upper and the lower part of
the body together.

This is part of an electronics enclosure.
*/


// use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>


$fa = 2;
$fs = 0.25;


/* The cylindrical element where the screw is.

The hole and the material must be realized separately, because of limitations
of OpenSCAD.
*/
module body_screw_column(
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    position = "front-left",
    end_dist = 10,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    head_through_d = screw_head_through_diameter(head_diameter);
    outer_d = head_through_d + 2 * shell_wall_thickness;

    cylinder(d=outer_d, h=shell_height);
}
%body_screw_column();


/* The hole for the screw.

The hole and the material must be realized separately, because of limitations
of OpenSCAD.
*/
module body_screw_hole(
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    position = "front-left",
    end_dist = 10,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=1
) {
    ds = 0.02;

    head_through_d = screw_head_through_diameter(head_diameter);
    screw_through_d = screw_thread_through_diameter(screw_diameter);
    screw_bite_d = screw_bite_diameter(screw_diameter);
    outer_d = head_through_d + 2 * shell_wall_thickness;

    conus_h = (head_through_d - screw_through_d) / 2;
    screw_through_h = shell_wall_thickness;
    // screw_through_h = max(shell_wall_thickness, screw_diameter);

    // Upper part of the hole, which is wide enough for the head of the
    // screw to pass through.
    translate([0, 0, partition_height + screw_through_h + conus_h - ds/2])
    cylinder(
        d=head_through_d,
        h=shell_height - partition_height - screw_through_h - conus_h + ds
    );

    // Middle part were screw goes through. Its wall conducts the clamping
    // force of the screw.
    // Conical part
    translate([0, 0, partition_height + screw_through_h - ds/2])
    cylinder(d1=screw_through_d, d2=head_through_d, h=conus_h + ds);
    // Short cylindrical part
    translate([0, 0, partition_height - ds/2])
    cylinder(d=screw_through_d, h=screw_through_h + ds);

    // The lower part, were the screw bites into.
    translate([0, 0, -ds/2])
    cylinder(d=screw_bite_d, h=partition_height + ds);
}
body_screw_hole();


/* The translation, that puts the elements into the right place.

The hole and the material must be realized separately, because of limitations
of OpenSCAD.
*/
module body_screw_translation(
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    // Possible values:  "front-left", "front-right", "rear-left", "rear-right"
    position = "front-left",
    end_dist = 10,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    head_through_d = screw_head_through_diameter(head_diameter);
    outer_d = head_through_d + 2 * shell_wall_thickness;

    tx = position == "front-left" || position == "rear-left"
       ? outer_d / 2 + end_dist
       : shell_length - outer_d / 2 - end_dist;

    ty = position == "front-left" || position == "front-right"
       ? outer_d / 2
       : shell_width - outer_d / 2;

    translate([tx, ty, 0]) {
        children();
    }
}


// Test geometry
module test_geometry() {
    difference() {
        union() {
            // create the shell
            difference() {
                body_shell_outer();
                body_shell_inner();
            }

            // The material for screwing parts together
            body_screw_translation(position="rear-right")
            body_screw_column();
        }

        // The holes for the screws
        body_screw_translation(position="rear-right")
        body_screw_hole();
    }
}
%test_geometry();
