/******************************************************************************
        Complete geometry to join the body's top and bottom parts
                            Electronics Enclosure
*******************************************************************************

Here are geometric elements to join the two haves of the body.

This is part of an electronics enclosure.
*/


use <helpers.scad>
// use <boxes.scad>
use <base-geometry.scad>
use <body-join-mid.scad>
use <body-join-end.scad>


$fa = 2;
$fs = 0.25;

/* The positive geometry to join the two parts of the body together.

    The holes are separate.
    The laches and the openings for them are also separate.
*/
module body_join_matter(
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=2.0
) {
    body_end_matter(
        screw_diameter=screw_diameter,
        head_diameter=head_diameter,
        partition_height = partition_height,
        position = "right",
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
    body_end_matter(
        screw_diameter=screw_diameter,
        head_diameter=head_diameter,
        partition_height = partition_height,
        position = "left",
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
    body_seam(
        position="front",
        partition_height = partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_wall_thickness=shell_wall_thickness
    );
    body_seam(
        position="rear",
        partition_height = partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_wall_thickness=shell_wall_thickness
    );
}


/* The negative geometry to join the two parts of the body together.

    The matter is in a separate module.
    The laches and the openings for them are also separate.
*/
module body_join_holes(
    screw_diameter=2.9,
    head_diameter=5.5,
    partition_height = 12,
    shell_length=200,
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=2.0
) {
    // Left
    body_end_holes(
        screw_diameter=screw_diameter,
        head_diameter=head_diameter,
        partition_height = partition_height,
        position = "left",
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
    // Right
    body_end_holes(
        screw_diameter=screw_diameter,
        head_diameter=head_diameter,
        partition_height = partition_height,
        position = "right",
        shell_length=shell_length,
        shell_width=shell_width,
        shell_height=shell_height,
        shell_radius=shell_radius,
        shell_wall_thickness=shell_wall_thickness
    );
}


/* The latches to join the two parts of the body together.

    The module creates the laches and the openings for them.

    Openings are created by setting `enlarged=true` and subtracting the enlarged
    geometry from the upper part of the body.
*/
module body_join_latches(
    enlarged=false,
    partition_height = 12,
    shell_length=200,
    shell_width=100,
    shell_wall_thickness=2.0
) {
    body_seam_latch(
        enlarged=enlarged,
        position="front",
        partition_height = partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_wall_thickness=shell_wall_thickness
    );
    body_seam_latch(
        enlarged=enlarged,
        position="rear",
        partition_height = partition_height,
        shell_length=shell_length,
        shell_width=shell_width,
        shell_wall_thickness=shell_wall_thickness
    );
}


// Test geometry
module test_geometry() {
    // end_pos = "left";
    end_pos = "right";

    difference() {
        union() {
            // create the shell
            %difference() {
                body_shell_outer(shell_wall_thickness=2.0);
                body_shell_inner(shell_wall_thickness=2.0);
            }

            body_join_matter();
            #body_join_holes();
            #body_join_latches();
        }

        // body_end_holes(position=end_pos);
    }
}
test_geometry();
