//##############################################################################
//                               Helpers
//##############################################################################
//
// Helper functions and modules


// Number Series
// ----------------------------------------------------------------------------

/* Create a series of regularly spaced numbers (1D coordinates)

The series is centered on the interval 0..length, and is monotonically
increasing.
*/
function regular_pattern_centered(length=100, pitch=20) =
    let(
        n_elem = floor(length / pitch),
        rest = length % pitch,
        dx = pitch / 2 - rest / 2
    )
    [ for (i = [1 : 1 : n_elem]) i * pitch - dx ]
;
// echo( "regular pattern: ", regular_pattern_centered(100, 30));
// echo( "regular pattern: ", regular_pattern_centered(1, 0.4));
// echo( "regular pattern: ", regular_pattern_centered(1, 0.5));
// echo( "regular pattern: ", regular_pattern_centered(100, 60));
// echo( "regular pattern: ", regular_pattern_centered(100, 110));


/* Create a series of numbers that lie in between the numbers of an existing series.

The resulting series is one element shorter than the old series.
*/
function pattern_in_between(
    input_pattern=[2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
) =
[ for (i = [0 : 1 : len(input_pattern) - 2])
    (input_pattern[i] + input_pattern[i+1]) / 2
]
;
// echo( "regular pattern: ", pattern_in_between([2, 4, 6, 8, 10, 12, 14, 16, 18, 20]));
// echo( "regular pattern: ", pattern_in_between([10, 12, 14, 16]));


/* Create a series of numbers that takes every N-th element of an existing series.

Tries to center the new pattern on the grid a much as possible.
*/
function grid_pattern_centered(
    grid=[2, 4, 6, 8, 10, 12, 14, 16, 18, 20],
    pitch=9
) = let(
    grid_pitch = grid[1] - grid[0],
    step = max(1, floor(pitch / grid_pitch)),
    n_steps = floor(len(grid) / step),
    rest_steps = floor(len(grid) % step),
    di = max(0, floor(step / 2 - rest_steps / 2))
)
[ for (i = [0 : 1 : n_steps - 1]) grid[i * step + di] ]
;
// echo("grid pattern", grid_pattern_centered(pitch=2));
// echo("grid pattern", grid_pattern_centered(pitch=4));
// echo("grid pattern", grid_pattern_centered(pitch=6));
// echo("grid pattern", grid_pattern_centered(pitch=9));
// echo("grid pattern", grid_pattern_centered(pitch=10));
// echo("grid pattern", grid_pattern_centered(pitch=12));


// Basic geometries
// -----------------------------------------------------------------------------

// 2D
// ---------------------------------------
/* Create a circular wedge, or pizza slice, with the tip at the origin. */
module circle_sector(r=10, angle=30, $fa=$fa){
    n_points = ceil(angle / $fa);
    angle_1 = angle / n_points;

    angles = [for (i = [0:n_points]) i * angle_1];
    coords = [
        [0.0, 0.0],
        for (th=angles) [r*cos(th), r*sin(th)]
    ];
    polygon(coords);
 }
//  circle_sector();


/* 2D polygon that consists of a row of triangles. */
module teeth_2d(
    teeth_size=2,
    teeth_length=160
) {
    n_teeth = floor(teeth_length / teeth_size);
    teeth_length_real = teeth_size * n_teeth;
    teeth_thick = teeth_size / 2;

    polygon([
        [teeth_length_real, 0],
        [teeth_length_real, -0.1],
        [0,                 -0.1],
        [0,                 0],
        for (i = [0:n_teeth - 1]) each [
            [(i + 0.5) * teeth_size, teeth_thick],
            [(i + 1)   * teeth_size, 0],
        ]
    ]);
}
// teeth_2d();


// 3D
// ---------------------------------------
module rounded_bar(length=10, width=2, height=3) {
    translate([width/2, width/2, 0])
    hull() {
        cylinder(d=width, h=height);
        translate([length - width, 0, 0])
        cylinder(d=width, h=height);
    }
}
// rounded_bar();


// Transformations
// -----------------------------------------------------------------------------

/*
A mirror module that retains the original object in addition to the mirrored
one.

Taken from:
    https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Tips_and_Tricks#Create_a_mirrored_object_while_retaining_the_original

Parameters
----------

v: vector[3]
    The mirror vector.
*/
module mirror_copy(v=[1, 0, 0]) {
    children();

    mirror(v)
    children();
}

/*
A rotation module that copies the original object and applies a cumulative
rotation to each copy.

Parameters
----------

r: vector[3]
    The rotation vector.

n: integer
    The number of result objects.
    The first object is not rotated.
*/
module rotate_copy(r=[10, 10, 10], n=2) {
    if (n <= 1) {
        children();
    } else {
        children();

        rotate(r)
        rotate_copy(r, n - 1) {
            children();
        }
    }
}

/*
A translation module that copies the original object and applies a cumulative
translation to each copy.

Parameters
----------

t: vector[3]
    The translation vector.

n: integer
    The number of result objects.
    The first object is not rotated.
*/
module translate_copy(t=[10, 10, 10], n=2) {
    if (n <= 1) {
        children();
    } else {
        children();

        translate(t)
        translate_copy(t, n - 1) {
            children();
        }
    }
}
