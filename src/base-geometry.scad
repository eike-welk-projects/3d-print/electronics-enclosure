/******************************************************************************
                    Basic Geometries - Electronics Enclosure
*******************************************************************************

Here are geometric elements that are shared between all parts of the enclosure.

This is part of an electronics enclosure.
*/


use <helpers.scad>
use <boxes.scad>


$fa = 2;
$fs = 0.25;


// Screw definitions --------------------------------------
/* Diameter of a hole, where the **head** of the screw must pass through. */
function screw_head_through_diameter(head_diameter) =
    head_diameter * 1.2;

/* Diameter of a hole, where the **thread** of the screw must pass through. */
function screw_thread_through_diameter(thread_diameter) =
    thread_diameter * 1.2;

/* Diameter of a hole, where the **thread** of the screw bites into and holds
 * securely. */
function screw_bite_diameter(thread_diameter) =
    thread_diameter * 0.85;

/* Minimum length of a hole, where the **thread** of the screw bites into and holds
 * securely. */
function screw_bite_min_length(screw_diameter) =
    screw_diameter * 2.5;


// Basic shape of the body --------------------------------
/* The outer surface of the body shell */
module body_shell_outer (
    length=200,
    width=100,
    height=50,
    radius=10,
    shell_wall_thickness=5,
    $fa=$fa,
    $fs=$fs
) {
    translate([0, 0, height])
    rotate([0, 90, 0])
    roundedCube([height, width, length,], r=radius, sidesonly=true, center=false);
}
// body_shell_outer();


/* The minimum inner radius of the body shell. */
function body_shell_inner_radius_min() = 1.0;

/* The inner surface of the body shell */
module body_shell_inner (
    length=200,
    width=100,
    height=50,
    radius=10,
    shell_wall_thickness=5,
    $fa=$fa,
    $fs=$fs
) {
    inner_r = radius - shell_wall_thickness;
    edge_r = max(inner_r, body_shell_inner_radius_min());

    translate([
        0      - 0.01,
        0      + shell_wall_thickness,
        height - shell_wall_thickness
    ])
    rotate([0, 90, 0])
    roundedCube(
        [
            height - 2*shell_wall_thickness,
            width  - 2*shell_wall_thickness,
            length + 0.02,
        ],
        r=edge_r,
        sidesonly=true,
        center=false);
}
// body_shell_inner();


// Ribs ---------------------------------------------------
/* Compute the height of a rib, from the width and height of the enclosure.
*/
function get_rib_height(shell_width, shell_height=0) =
    0.06 * max(shell_width, shell_height);


/* A rib for reinforcement of the shell.

A reinforcing structure that runs transversely.
*/
module rib(
    // Dimensions of the rib itself
    width=3,
    height=5,
    // Center the rib in X-Direction
    center=false,
    // Dimensions of the body shell where the rib is located
    shell_width=100,
    shell_height=50,
    shell_radius=10,
    shell_wall_thickness=5
) {
    ds = 0.02;
    tx = center ? - width/2 : 0;

    translate([
        tx,
        shell_wall_thickness - ds,
        shell_wall_thickness - ds
    ])
    difference() {
        // Outer circumference of the rib
        body_shell_outer(
            length=width,
            width= shell_width  - 2*shell_wall_thickness + 2*ds,
            height=shell_height - 2*shell_wall_thickness + 2*ds,
            radius=shell_radius - shell_wall_thickness + ds,
            shell_wall_thickness=height + ds,
            $fa=$fa,
            $fs=$fs
        );
        // Inner hole of the rib
        body_shell_inner(
            length=width,
            width= shell_width  - 2*shell_wall_thickness + 2*ds,
            height=shell_height - 2*shell_wall_thickness + 2*ds,
            radius=shell_radius - shell_wall_thickness + ds,
            shell_wall_thickness=height + ds,
            $fa=$fa,
            $fs=$fs
        );
    };
}
// rib();


// Test geometry
module test_geometry() {
    // create the shell
    %difference() {
        body_shell_outer();
        body_shell_inner();
    }

    // One rib
    translate([190 - 3, 0, 0])
    rib();

    // // One stringer
    // stringer();

    // One set of vent openings on each side of the shell
    opening_quad();
}
test_geometry();
