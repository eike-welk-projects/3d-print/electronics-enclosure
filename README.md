Electronics Enclosure
=====================

This is a small, fully parametric, electronics enclosure, that can be 3D printed. 
It is very well vented, and is therefore suitable for power supplies.

The enclosure is written in the CAD language *OpenSCAD*.

This project is at the limit, of what can be usefully done with *OpenSCAD*: The
preview of all parts together takes 6 seconds, to render all parts, takes about
6 minutes.


3MF
-----------

The `*.3mf` files are sliced for 0.6 mm nozzle diameter, adaptive layer
height, and PETG. No supports are necessary. They have been printed with a
*Prusa MK3s*.


OpenSCAD
-----------

The `*.scad` files are a program for the CAD language *OpenSCAD*. They depend on
each other. Together they are a computer program, that defines the geometry of
the enclosure.

There are currently two main programs:

* `src/z-enclosure-print.scad`: The enclosure configured for the small LED power
  supply "MEAN WELL APV-8-12". There is a hole for a power switch on the front,
  the cables exit on the rear.

* `src/z-enclosure-print-mw-lpv-35-12.scad` The enclosure configured for the LED
  power supply "MEAN WELL LPV-35-12". There is no power switch, the cables exit
  on both ends.

  This configuration is a bit flimsy. It would benefit from higher ribs (bigger
  factor in function `get_rib_height`), or additional screws to hold the
  enclosure together.

Both files contain all parts of the enclosure arranged for printing.


Links
----------

The project is also on Printables:

https://www.printables.com/model/228865-electronics-enclosure
